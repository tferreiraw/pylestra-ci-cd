FROM python:3.7

ENV PYTHONUNBUFFERED 1

WORKDIR /srv/app

EXPOSE 8000

COPY requirements.txt /srv/app/

RUN pip install --no-cache-dir -r requirements.txt

COPY . /srv/app/
